# Andrew's Blocklists

> **tl;dr** - **Chibi** for basic blocking, **Kouhai** for balanced blocking, ~~**Senpai** if you want a little more oomph~~. **Senpai is identical to Kouhai as of Sept 6, 2022**.

**My blocklists**:

* **Block** ads, analytics/trackers, malware, ransomware, phishing, malvertising, mobile ads/tracking, fake news (I hate fake news), the Luminati/Hola network, cryptominers, scam retailers, CNAME disguised trackers, and Windows telemetry
* **Reduce** page load times and data/CPU usage
* **Increase** security, privacy, and battery life on portable devices
* Are **lean** thanks to a daily check and removal of any dead/invalid domains
* Are an **aggregate of well-curated blocklists** with minimal false positives
* Are **updated daily** at ~2:00am Eastern Time
* Are **available in different flavours**:
  * **Non-Strict** with Rakuten (*formerly Ebates*) and RedFlagDeals affiliate domains allowed for a great shopping experience (dat cashback dough)
  * **Strict** with Rakuten/RedFlagDeals affiliate domains blocked
* Include a **compressed format** optimized for use with OpenWRT [adblock](https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md) or [simple-adblock](https://github.com/openwrt/packages/blob/master/net/simple-adblock/files/README.md)

License: MIT (https://mit-license.org/)

## Lists

> Last updated **2025-03-07 02:00:01** with [**75567** dead/invalid domains](https://gitlab.com/andryou/block/raw/master/dead) removed

| Name | Description | Download | # | Download | # |
|:---------:|-----------|:-----------:|:-----------:|:-----------:|:--------:|
Chibi | Basic protection. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi) (1.5M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-domains) (1.1M) | 51946 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-compressed) (1.3M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-compressed-domains) (904K) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/chibi-unbound) (2.2M) [**DNSMASQ**](https://gitlab.com/andryou/block/raw/master/chibi-dnsmasq) (1.4M) [**ADBLOCK PLUS**](https://gitlab.com/andryou/block/raw/master/chibi-adblockplus) (1.1M) | 43584 domains |
Chibi (**Strict**) | ^ with Rakuten and RedFlagDeals affiliate links **blocked**. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-strict) (1.5M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-domains) (1.1M) | 51980 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-compressed) (1.3M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-compressed-domains) (904K) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/chibi-strict-unbound) (2.2M) [**DNSMASQ**](https://gitlab.com/andryou/block/raw/master/chibi-strict-dnsmasq) (1.4M) [**ADBLOCK PLUS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-adblockplus) (1.1M) | 43460 domains |
Kouhai | Balanced protection. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai) (3.1M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-domains) (2.3M) | 107381 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-compressed) (2.0M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-compressed-domains) (1.5M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/kouhai-unbound) (3.6M) [**DNSMASQ**](https://gitlab.com/andryou/block/raw/master/kouhai-dnsmasq) (2.2M) [**ADBLOCK PLUS**](https://gitlab.com/andryou/block/raw/master/kouhai-adblockplus) (1.7M) | 72514 domains |
Kouhai (**Strict**) | ^ with Rakuten and RedFlagDeals affiliate links **blocked**. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict) (3.1M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-domains) (2.3M) | 107419 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-compressed) (2.0M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-compressed-domains) (1.5M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-unbound) (3.5M) [**DNSMASQ**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-dnsmasq) (2.2M) [**ADBLOCK PLUS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-adblockplus) (1.7M) | 72358 domains |

## Usage

* **HOSTS Format** - Blokada, DNS66, AdAway, Hostsman, dnsmasq (via `addn-hosts`), Diversion
* **DOMAINS Format** - Pi-Hole, Diversion
* **COMPRESSED HOSTS Format** - OpenWRT [adblock v2.4.0 or newer](https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md) (see [Configuring adblock](#configuring-adblock) below) or [simple-adblock v1.6.3 or newer](https://docs.openwrt.melmac.net/simple-adblock/) (under `Blacklisted Hosts URLs`)
  * If using **adblock** >= v3.8.0, ensure "DNS Blocking Variant" (`adb_dnsvariant` in the config file) is set to `nxdomain` (default: `nxdomain`)
  * If using **Simple AdBlock** >= v1.8.0, ensure "DNS Service" under "Advanced Configuration" (`dns` in the config file) is set to `dnsmasq.conf` ("DNSMASQ Config") or `dnsmasq.servers` ("DNSMASQ Servers File"), or `unbound.adb_list` ("Unbound AdBlock List") if using unbound (default: `dnsmasq.servers`)
* **COMPRESSED DOMAINS Format** - compatible with **Simple AdBlock** under `Blacklisted Domain URLs` (*not Blacklisted Hosts URLs*)
* **UNBOUND Format** - add `include: "/path/to/file/kouhai-unbound"` (for example) to `/etc/unbound/unbound.conf` and then run `unbound-control reload`. This command must be run after every list update.
* **DNSMASQ Format** - add `conf-file=/path/to/blocklist` to `dnsmasq.conf` (or if `dnsmasq.conf` already contains a `conf-file=` line loading all `*.conf` files in a folder, download the blocklist into that folder with a .conf extension) and then run `service dnsmasq restart` (or equivalent, eg. `/etc/init.d/dnsmasq restart` for OpenWRT). This command must be run after every list update.
* **ADBLOCK PLUS Format** - AdGuard (Home), most browser adblock solutions (eg. uBlock Origin)

### Configuring OpenWRT adblock

The adblock module for OpenWRT comes with the Kouhai (non-strict compressed domains) list included as an option.

If you want to use a different list above you will need to follow the [steps](https://github.com/openwrt/packages/tree/master/net/adblock/files#examples) under "Edit, add new adblock sources".

The below example configuration lines assume your adblock is configured to use TLD compression (adblock v2.4.0+ and if >= v3.8.0, "DNS Blocking Variant" (`adb_dnsvariant`) is set to `nxdomain`). If not, remove `-compressed` from the URL strings below.

If you would like to use the **Strict** version of a list above, use the appropriate link for **DOMAINS** (if TLD compression is unavailable) or **COMPRESSED DOMAINS** (if TLD compression is available) as the `url` value.

```
	"andryou-chibi": {
		"url": "https://gitlab.com/andryou/block/raw/master/chibi-compressed-domains",
		"rule": "/^([[:alnum:]_-]{1,63}\\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower($1)}",
		"size": "M",
		"focus": "compilation",
		"descurl": "https://gitlab.com/andryou/block/-/blob/master/readme.md"
	},
	"andryou-chibi-strict": {
		"url": "https://gitlab.com/andryou/block/raw/master/chibi-strict-compressed-domains",
		"rule": "/^([[:alnum:]_-]{1,63}\\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower($1)}",
		"size": "M",
		"focus": "compilation",
		"descurl": "https://gitlab.com/andryou/block/-/blob/master/readme.md"
	},
	"andryou-strict": {
		"url": "https://gitlab.com/andryou/block/raw/master/kouhai-strict-compressed-domains",
		"rule": "/^([[:alnum:]_-]{1,63}\\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower($1)}",
		"size": "L",
		"focus": "compilation",
		"descurl": "https://gitlab.com/andryou/block/-/blob/master/readme.md"
	},
```

**Important**: only enable one of the lists. As you can see below, there is overlap.

### Sources

| List / Site | Description | License | Chibi | Kouhai | ~~Senpai~~ |
|---------|-----------|:-----------:|:-----------:|:-----------:|:-----------:|
[Andrew's Blocklist](https://gitlab.com/andryou/andrews-settings/raw/master/andrewblocklist) ([site](https://gitlab.com/andryou/andrews-settings)) | My personal blocklist with any nasty domains I come across. | - | ✔ | ✔ | ✔ |
[Fake News](https://raw.githubusercontent.com/marktron/fakenews/master/fakenews) ([site](https://github.com/marktron/fakenews/)) | An in-progress collection of fake news outlets. | MIT | ✔ | ✔ | ✔ |
[lightswitch05 Hate & Junk](https://www.github.developerdan.com/hosts/lists/hate-and-junk-extended.txt) ([site](https://github.com/lightswitch05/hosts)) | This is an opinionated list to block things that I consider to be hateful or just plain junk. | Apache-2.0 | ✔ | ✔ | ✔ |
[Luminati/Hola Block](https://raw.githubusercontent.com/durablenapkin/block/master/luminati.txt) ([site](https://github.com/durablenapkin/block)) | Block the Luminati/Hola network. | MIT | ✔ | ✔ | ✔ |
[Streaming Ads Block](https://raw.githubusercontent.com/durablenapkin/block/master/streaming.txt) ([site](https://github.com/durablenapkin/block)) | Block streaming ads/pop-ups/pop-unders. | MIT | ✔ | ✔ | ✔ |
[NoCoin](https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt) ([site](https://github.com/hoshsadiq/adblock-nocoin-list)) | This is an adblock list to block "browser-based crypto mining". | MIT | ✔ | ✔ | ✔ |
[WindowsSpyBlocker](https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt) ([site](https://github.com/crazy-max/WindowsSpyBlocker)) | Block spying and tracking on Windows systems. | MIT | ✔ | ✔ | ✔ |
[Scam Blocklist](https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/hosts.txt) ([site](https://github.com/durablenapkin/scamblocklist)) | A blocklist to protect users against untrustworthy retail sites. | MIT | ✔ | ✔ | ✔ |
[MVPS hosts file](https://raw.githubusercontent.com/StevenBlack/hosts/master/data/mvps.org/hosts) ([site](https://github.com/StevenBlack/hosts/tree/master/data/mvps.org)) | The purpose of this site is to provide the user with a high quality custom HOSTS file. | CC BY-NC-SA 4.0 | ✔ | ✔ | ✔ |
[yoyo.org](https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext&useip=0.0.0.0) ([site](https://pgl.yoyo.org/adservers/)) | Blocking with ad server and tracking server hostnames. | - | ✔ | ✔ | ✔ |
[Dan Pollock – someonewhocares](https://someonewhocares.org/hosts/zero/hosts) ([site](https://someonewhocares.org/hosts/)) | How to make the internet not suck (as much). | non-commercial with attribution | ✔ | ✔ | ✔ |
[URLHaus](https://urlhaus.abuse.ch/downloads/hostfile/) ([site](https://urlhaus.abuse.ch/)) | A project from abuse.ch with the goal of sharing malicious URLs that are being used for malware distribution. | CC0 | ✔ | ✔ | ✔ |
[AdGuard CNAME Disguised Trackers](https://raw.githubusercontent.com/AdguardTeam/cname-trackers/master/combined_disguised_trackers_justdomains.txt) ([site](https://github.com/AdguardTeam/cname-trackers)) | The list of trackers that disguise the real trackers by using CNAME records. | MIT | ✔ | ✔ | ✔ |
[StevenBlack Unified Hosts](https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts) ([site](https://github.com/StevenBlack/hosts)) | This hosts file is a merged collection of hosts from reputable sources, with a dash of crowd sourcing via Github. | MIT |  | ✔ | ✔ |
~~[lightswitch05 Ads & Tracking](https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt) ([site](https://github.com/lightswitch05/hosts))~~ **Removed Sept 6, 2022 due to [no maintenance](https://github.com/lightswitch05/hosts/issues/356)** | ~~A programmatically expanded list of hosts found to not be on other lists.~~ | Apache-2.0 |  |  | ❌ |
